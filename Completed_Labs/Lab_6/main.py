"""
@file \Lab_6\main.py

This file runs one independent task to run the front-end UI on a laptop. 

@author: Clara Brechtel

@date: 11/23/2020 
"""

from TaskUI import TaskUI


## TaskUI takes user input and reads from the UART of a Nucleo board
task1 = TaskUI(1, 0.05, dbg = False)

## The task list contains the tasks to be run "round-robin" style
taskList = [task1]

while True:
    for task in taskList:
        task.run()