'''
@file TaskUI.py
@brief User interface task as a finite state machine
@author Clara Brechtel
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date 11/23/2020
'''

import time
import serial
import matplotlib.pyplot as plt
import fastnumbers

class TaskUI:
    '''
    User interface task.
    
    An object of this class interacts with serial port UART2 waiting for 
    integers. Each integer recieved is read by the serial and placed into 
    an array, one array contains time values and the other array stores position.
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_QUERY_USER       = 1
        
    ## Parse data 
    S2_PARSE_DATA       = 2

    def __init__(self, taskNum, interval, dbg=True):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## Initialize empty array to hold time values 
        self.enc_time = []
        
        ## Initialize empty array to hold position values
        self.velocity = []
        
        ## Holds the user kp input variable
        self.Kp = 0 
        
        ## Holds the user reference variable input 
        self.V_ref = []
        
        ## The number of seconds since Jan 1, 1970
        self.start_time = time.time()
        
        ## The "timestamp" for when to run the task next 
        self.next_time = self.start_time + self.interval 
        
        ## Serial port
        self.ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
        
        if self.dbg:
            print('Created user interface task')

    def run(self):
        '''
        Runs one iteration of the task
        '''

        ## Updates the current time stamp 
        self.curr_time = time.time()
        if (self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_QUERY_USER)
            
            elif(self.state == self.S1_QUERY_USER):
                self.printTrace()
                # Run State 1 Code
                self.V_ref = self.promptVref()
                if(self.V_ref >= 1):
                    self.Kp = self.promptKp()
                    if(self.Kp > 0):
                        self.ser.reset_input_buffer()
                        self.transitionTo(self.S2_PARSE_DATA)
                    
            elif(self.state == self.S2_PARSE_DATA):
                self.printTrace()
                # Run State 2 Code
                if (self.ser.inWaiting() > 4): 
                    time.sleep(5)
                    for i in range(151):
                        enc_time, velocity = self.collect_data()
                        self.enc_time.append(enc_time)
                        self.velocity.append(velocity)
                        if (abs(self.V_ref - velocity) <= 5 or i == 151):
                            print(enc_time)
                            print(velocity)
                            plt.plot(self.enc_time, self.velocity, label='V_real')
                            plt.plot([self.enc_time[1],self.enc_time[-1]], [self.V_ref,self.V_ref], label="V_ref")
                            plt.legend(fancybox=True,loc='upper right')
                            plt.title("Motor Step Response, Kp = {}".format(self.Kp))
                            plt.xlabel("Time (seconds)")
                            plt.ylabel("Velocity (rad/s)")
                            plt.show()
                            plt.savefig("motor_encoder_data.png")
                            self.transitionTo(self.S1_QUERY_USER)
                            self.enc_time = []
                            self.velocity = []
                            self.ser.flushInput()
                            break
                        break
        
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time += self.interval 
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, (self.curr_time - self.start_time))
            print(str)
            
    def promptVref(self):
        '''
        Query user for input 
        '''
        
        V_ref = input('Input list of reference velocities separated by commas (max = five values): ')
        for i in range(len(V_ref)):
            self.ser.write(V_ref[i].encode('ascii'))

        return int(V_ref) 
    
    def promptKp(self):
        '''
        Query user to stop recording data.

        '''

        Kp = input('Input the proportional gain as an integer: ')
        self.ser.write(Kp.encode('ascii'))
     
        return int(Kp)
        
    
    def collect_data(self):
        '''
        Reads data from serial and converts it to an integer value. 

        '''
        enc_time = 0
        velocity = 0
        line_string = self.ser.readline().decode('ascii')
        line_list = line_string.strip('\r\n').split(',')
        if fastnumbers.isfloat(line_list[0], str_only=False):
            enc_time = float(line_list[0])     
        else:
            pass
            
        if fastnumbers.isfloat(line_list[1], str_only=False):
            velocity = float(line_list[1])
        else:
            pass
        
        return enc_time, velocity