# -*- coding: utf-8 -*-
'''
@file \Lab_6\Firmware\TaskController.py
@brief A task that controls and sets the time for three independent objects; a motor encoder, motor driver, and closed loop system 
@author Clara Brechtel 
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date November 23, 2020
'''

import utime 
import shares 

class TaskController:
    '''
    @brief This class controls the timing and communication between three objects to control the speed of a motor 
    '''
    ## Initialization state
    S0_INIT = 0
    
    ## Wait for user input 
    S1_GET_INPUT = 1
    
    ## Compute data 
    S2_RUN = 2 
    
    def __init__(self, taskNum, interval, MotorEncoder, MotorDriver, ClosedLoop, dbg=False):
        '''
        @brief Creates a controller object 

        '''
        ## The name of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the Motor Encoder object 
        self.MotorEncoder = MotorEncoder
        
        ## A class attribute "copy" of the Motor Driver object
        self.MotorDriver = MotorDriver 
        self.MotorDriver.enable()
        
        ## A class attribute "copy" of the Closed Loop object 
        self.ClosedLoop = ClosedLoop 
        
        ## Initialize measured velocity to 0 
        self.V_real = 0
        
        ## Initialize reference velocity to 0 
        self.V_ref = 0 
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
        
        ## Initialize duty from previous run to 0 
        self.duty_old = 0
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        if self.dbg:
            print('Created scaler task')
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()

        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                self.transitionTo(self.S1_GET_INPUT)
                
            elif(self.state == self.S1_GET_INPUT):
                self.printTrace()
                if(shares.V_ref):
                    self.V_ref = shares.V_ref 
                    if(shares.Kp):
                        self.ClosedLoop.set_kp(shares.Kp)
                        self.transitionTo(self.S2_RUN)
                    
            elif(self.state == self.S2_RUN):
                self.printTrace()
                
                self.MotorEncoder.update(0.05)
                self.V_real = self.MotorEncoder.get_speed()
                shares.V_real = self.V_real
                shares.enc_time = utime.ticks_ms()*1E-3 

                duty_new = self.ClosedLoop.update(self.V_ref, self.V_real)
                duty_current = self.duty_old + duty_new 
                if duty_current > 100:
                    duty_current = 100
                elif duty_current < 0:
                    duty_current = 0 
                self.duty_old = duty_current 
                self.MotorDriver.set_duty(duty_current)
            
                if(abs(self.V_ref - self.V_real) <= 2):
                    shares.V_real = 'Stop'
                    self.transitionTo(self.S0_INIT)
                    self.MotorDriver.set_duty(0)
                    shares.V_ref = None
                    shares.Kp = None 

                else:
                    pass
                
            else:
                pass 
                
            self.runs += 1
                
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
    
            
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
        
        
        
        
        
        
        
        
        
        
        
        
        
        