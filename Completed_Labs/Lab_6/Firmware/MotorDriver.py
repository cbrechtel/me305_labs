# -*- coding: utf-8 -*-
'''
@file \Lab_6\Firmware\MotorDriver.py
@brief A task that sets up a motor and provides functionality to control the motors speed. 
@author Clara Brechtel 
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date 11/23/2020
'''

import pyb 

class MotorDriver:
    '''
    @brief This class implements a motor driver for two DC motors on the ME 305 board.
    '''
    
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        '''
        @brief Creates a motor driver by initializing GPIO pins and turning the motor off. 
        @param nSLEEP_pin   A pyb.Pin object to user as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1. 
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param timer        A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin. 
        '''
        
        print('Creating a motor driver object.')
        
        ## Create pin object for nSLEEP and initialize to low 
        self.nSLEEP = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        self.nSLEEP.low() 
        
        ## Create pin object for IN1 and initialize to low 
        self.IN1 = pyb.Pin(IN1_pin[0], pyb.Pin.OUT_PP)
        self.IN1.low()
        
        ## Create pin object for IN2 and initialize to low 
        self.IN2 = pyb.Pin(IN2_pin[0], pyb.Pin.OUT_PP)
        self.IN2.low()
        
        ## Create timer object and initialize frequency 
        self.tim = pyb.Timer(timer, freq=20000)
        
        ## Create channel object for IN1 pin to generate PWM and initialize to 0 
        self.tim_channel_1 = self.tim.channel(IN1_pin[1], pyb.Timer.PWM, pin=self.IN1)
        self.tim_channel_1.pulse_width_percent(0)
        
        ## Create channel object for IN2 pin to generate PWM and initialize to 0 
        self.tim_channel_2 = self.tim.channel(IN2_pin[1], pyb.Timer.PWM, pin=self.IN2)
        self.tim_channel_2.pulse_width_percent(0)
        
        print('All pins and timer initialized!')
        
    def enable(self):
        '''
        @brief Enable the motor driver by setting nSLEEP to high.

        '''
        print('Enabling Motor')
        self.nSLEEP.high()
        
        return 
    
    def disable(self):
        '''
        @brief Disable motor by setting nSLEEP to low. 
        
        '''
        print('Disabling Motor')
        self.nSLEEP.low()
        
        return 
    
    def set_duty(self, duty):
        '''
        @brief Set the duty cycle of the motor to the given level. Positive values 
        move the motor in the forward direction and negative values move the motor 
        in the opposite direction. 
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor. 

        '''
        
        try:
            duty = int(duty)
        except ValueError:
            print('Error! Please input an integer.')
            
        if duty > 0 and duty <= 100:
            self.tim_channel_2.pulse_width_percent(abs(duty))   # Forwards
        elif duty > 100:
            duty = 100
            self.tim_channel_2.pulse_width_percent(abs(duty))
        else:
            duty = 0
            self.tim_channel_2.pulse_width_percent(abs(duty))
            
        #print('Duty cycle set to {}'.format(duty))
            
        return 
        
        
        