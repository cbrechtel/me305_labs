"""
@file \Lab_6\Firmware\shares.py

This file stores global variables used between two tasks. 

@author: Clara Brechtel

@date: 11/23/2020 
"""

## The actuation signal defined by the user 
Kp = None

## The reference velocity defined by the user 
V_ref = None

## The velocity measured by the encoder 
V_real = None

## The time that the V_real value was measured 
enc_time = None 