'''
@file \Lab_6\Firmware\TaskUser.py
@brief A task that communicates data between the front-end UI and the Nucleo board 
@author Clara Brechtel 
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date 11/23/2020
'''

import utime
from pyb import UART 
import shares 

class TaskUser:
    '''
    TaskData records and prints the time and position of the motor encoder. 
    
    An object of this class encodes the position and time from the encoder. 
    
    '''

    ## Initialization state
    S0_INIT = 0
    
    ## Wait for reference array to be present in buffer
    S1_WAIT_FOR_REF_ARRAY = 1
    
    ## Wait for Kp to be present in buffer 
    S2_WAIT_FOR_KP = 2 
    
    ## Set the values for shares to communicate with the controller 
    S3_SET_SHARES = 3
    
    ## Recieve data from the controller 
    S4_RECIEVE_DATA = 4
    
    ## Send data to the front-end UI 
    S5_SEND_DATA = 5 
    
    def __init__(self, taskNum, interval, dbg=True):
        '''
        Creates a TaskData object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The name of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Intialize a byte array to store reference velocitys
        self.V_ref = bytearray(1)
        
        ## Initialize Kp to 0 
        self.Kp = 0
        
        ## Initialize reference index to 0 
        self.ref_index = 0
        
        ## Initialize index for array that stores measured velocity to 0 
        self.real_index = 0 
        
        ## Initialize empty time matrix, pre-allocate size for speed
        self.time = [None]*151
        
        ## Initialize empty V_real matrix, pre-allocate size for speed 
        self.V_real = [None]*151
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Set up UART port 
        self.myuart = UART(2)

        if self.dbg:
            print('Created scaler task')
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_REF_ARRAY)
            
            elif(self.state == self.S1_WAIT_FOR_REF_ARRAY):
                self.printTrace()
                # Run State 1 Code
                if(self.myuart.any()):
                    V_ref = self.myuart.read(4)
                    V_ref = V_ref.decode('ascii')
                    self.V_ref = int(V_ref)
                    self.transitionTo(self.S2_WAIT_FOR_KP)
                    self.myuart.sendbreak()
                         
            elif(self.state == self.S2_WAIT_FOR_KP):
                self.printTrace()
                # Run State 2 Code 
                if(self.myuart.any()):
                    Kp = self.myuart.read(2)
                    Kp = Kp.decode('ascii')
                    self.Kp = int(Kp)
                    if(self.Kp > 0):
                       self.transitionTo(self.S3_SET_SHARES)
                       shares.Kp = self.Kp
            
            elif(self.state == self.S3_SET_SHARES):
                self.printTrace()
                # Run State 3 Code 
                shares.V_ref = self.V_ref 
                self.transitionTo(self.S4_RECIEVE_DATA)
                    
            elif(self.state == self.S4_RECIEVE_DATA):
                self.printTrace()
                # Run state 4 code 
                if(shares.V_real == 'Stop'):
                    self.transitionTo(self.S5_SEND_DATA)
                elif(self.real_index == 151):
                    self.real_index = 0 
                    pass
                elif(isinstance(shares.V_real, float)):
                    self.V_real[self.real_index] = shares.V_real
                    self.time[self.real_index] = shares.enc_time 
                    self.real_index += 1 
                    pass
                    
            elif(self.state == self.S5_SEND_DATA):
                self.printTrace()
                # Run state 5 code 
                for i in range(len(self.V_real)):
                    self.myuart.write('{},{}\r\n'.format(self.time[i],self.V_real[i]))
                self.transitionTo(self.S1_WAIT_FOR_REF_ARRAY)
                self.time = [None]*151
                self.V_real = [None]*151
                self.V_ref = 0 
                self.Kp = 0 
                
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
    
