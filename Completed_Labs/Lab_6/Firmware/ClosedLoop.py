# -*- coding: utf-8 -*-
'''
@file \Lab_6\Firmware\ClosedLoop.py
@brief A class that implements a closed loop feedback system to set the speed of a motor 
@author Clara Brechtel 
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date November 23, 2020
'''


class ClosedLoop:
    '''
    @brief This class implements a first-order feedback loop 
    '''
    
    def __init__(self):
        '''
        @brief Construct ClosedLoop object and initialize state. 
        '''
        
        ## Initialize the proportional gain, Kp, to 1
        self.Kp = 0
        
        ## Initialize the reference velocity, V_ref to 0
        self.V_ref = 0
        
        ## Initialize the measured velocity, V_meas to 0
        self.V_real = 0
        
        ## Initialize the actuation signal, L, to 0
        self.duty = 0
                
    def update(self, V_ref, V_real):
        '''
        @brief  Updates the actuation signal 
        @param V_ref The reference velocity passed in by the user 
        @param V_real The actual velocity measured by the encoder 
        '''
        
        self.V_ref = V_ref
        self.V_real = V_real
        
 
        self.duty = self.Kp * (self.V_ref - self.V_real)
            
        return self.duty
    
    def set_kp(self, Kp):
        '''
        @brief  Updates Kp in the ClosedLoop object
        @param Kp Proportional gain provided by the user 

        '''
        self.Kp = Kp
        
        return
        
    def get_kp(self):
        '''
        @brief Return the most updated Kp value 
        @return Kp Returns the current proportional gain 
        '''
        
        return self.Kp 
        
        
        
        
        
        
        
        