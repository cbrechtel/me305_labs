"""
@file \Lab_6\Firmware\main.py

This file is uploaded onto the Nucleo microcontroller and creates an object of the motor encoder, motor driver, 
and closed loop system. This file defines all of the pins and timers used to control the motor. 

@author: Clara Brechtel

@date: 11/23/2020 

"""
import pyb
import MotorEncoder, MotorDriver, ClosedLoop 
from TaskController import TaskController 
from TaskUser import TaskUser

## Create pin object at pin PB6
pin1 = pyb.Pin.cpu.B6
## Create pin object at pin PB7
pin2 = pyb.Pin.cpu.B7

## Define timer number 
timer = 4

## Create Motor Encoder object
enc1 = MotorEncoder.MotorEncoder(pin1, pin2, timer)

## nSLEEP_pin contains pin value for sleep pin
nSLEEP_pin = pyb.Pin.cpu.A15

# List representing [pin, channel] for each pin used to run the motor 
## Motor 1 input pin B4 and channel 1
IN1_pin = [pyb.Pin.cpu.B4, 1]
## Motor 1 input pin B5 and channel 2 
IN2_pin = [pyb.Pin.cpu.B5, 2]
## Motor 2 input pin B0 and channel 3
IN3_pin = [pyb.Pin.cpu.B0, 3]
## Motor 2 input pin B1 and channel 4
IN4_pin = [pyb.Pin.cpu.B1, 4]

## Timer object used for PWM generation 
tim = 3

## Create a motor object for motor 1 with IN1, IN2, and timer 
mot1 = MotorDriver.MotorDriver(nSLEEP_pin, IN1_pin, IN2_pin, tim)

## Create a motor object for motor 2 with IN3, IN4, and timer 
# mot2 = MotorDriver(nSLEEP_pin, IN3_pin, IN4_pin, tim)

## Create a ClosedLoop object 
loop = ClosedLoop.ClosedLoop()

## (Task 1) TaskData records and prints the position of the encoder at each time point
task1 = TaskController(1, 50000, enc1, mot1, loop, dbg = False)

## (Task 2) TaskData records and prints the position of the encoder at each time point
task2 = TaskUser(2, 50000, dbg = False)

## The task list contains the tasks to be run "round-robin" style
taskList = [task1,
            task2]

while True:
    for task in taskList:
        task.run()