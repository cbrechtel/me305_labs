# -*- coding: utf-8 -*-
"""
@file TaskEncoder.py

This file constructs the TaskEncoder object which reads the position of a motor. 

@author: Clara Brechtel

@date: 10/19/2020 
"""

import shares
import utime 
import pyb 

class TaskEncoder:
    '''
    @brief      A finite state machine to read positions from a motor encoder.
    @details    This class implements a finite state machine to read the position
                of a motor from an encoder. 
    '''

    ## Constant defining State 0 - Initialization
    S0_INIT = 0
    
    ## Constant defining State 1 - Update encoder object
    S1_UPDATE = 1
    
    ## Constant defining State 2 - Wait for user input 
    S2_WAIT_FOR_CMD = 2
    
    def __init__(self, taskNum, interval, MotorEncoder):
        '''
        @brief      Creates a TaskEncoder object and initializes state. 
        '''
        
        ## The name of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        #self.dbg = dbg
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the Motor Encoder object 
        self.MotorEncoder = MotorEncoder 
        
        ## A class attribute "copy" of the "position" variable 
        #self.position = self.MotorEncoder.get_position()
        
        ## A class attribute "copy" of the "delta" variable 
        #self.delta = self.MotorEncoder.get_delta()
        
        ## A counter showing the number of times a task is run 
        self.runs = 0
        
        ## The number of seconds since Jan 1, 1970
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when to run the task next 
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Updates the current time stamp 
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_UPDATE)
                
            elif(self.state == self.S1_UPDATE):
                # Run State 1 Code
                if shares.cmd:
                    self.transitionTo(self.S2_WAIT_FOR_CMD)
                else:
                    self.MotorEncoder.update()
                    print(self.MotorEncoder.get_position())
                    
            elif(self.state == self.S2_WAIT_FOR_CMD):
                # Run State 2 Code
                if(shares.cmd == 122):
                    self.MotorEncoder.set_position()
                    shares.resp = 'Position Reset to 0'
                    
                elif(shares.cmd == 112):
                    shares.resp = 'Encoder position: ' + str(self.MotorEncoder.get_position())
                    
                elif(shares.cmd == 100):
                    shares.resp = 'Encoder delta: ' + str(self.MotorEncoder.get_delta())
                
                else:
                    pass

                shares.cmd = None
                self.transitionTo(self.S1_UPDATE)
                self.MotorEncoder.update()

            else:
                # Uh-oh state (undefined)
                # Error handling 
                pass 
        
            # Add runs here 
            self.runs += 1 
            
            #self.next_time += self.start_time + self.interval
            self.next_time = utime.ticks_add(self.next_time, self.interval)   # Updating the "scheduled" timestamp 
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

class MotorEncoder:
    '''
    @brief      A MotorEncoder class
    @details    This class represents an encoder on a motor. The motor is attached
                to two pins and one timer object. 
    '''
    
    
    def __init__(self, pin1, pin2, timer):
        '''
        @brief      Creates a motor object
        @param pin1  The first pin object that the motor is connected to
        @param pin2  The second pin object that the motor is connected to
        @param timer A timer object that the motor is connected to 
        '''
        
        ## The position of the encoder
        self.position = 0 
        ## The change in position of the encoder between two most recent updates
        self.delta = 0 
        
        ## The encoder timer object 
        self.tim=pyb.Timer(timer)
        self.tim.init(prescaler=0,period=65535)
        
        ## Connect the channels to the pins and timer 
        self.tim.channel(1,pin=pin1,mode=pyb.Timer.ENC_AB)
        self.tim.channel(2,pin=pin2,mode=pyb.Timer.ENC_AB)
        
        print('Two pin objects created attached to pin '+ str(pin1) + ' and ' + str(pin2))
        print('Timer object created attached to timer '+ str(timer))
    
    def update(self):
        '''
        @brief      Updates the recorded position of the encoder.
        '''
        
        #prev_position = self.position
        count = self.tim.counter()
        
        delta = count - self.position
        # logic to fix 'bad' delta values 
        if (abs(delta) > 32767): 
            if(delta < 0):
                self.delta = delta + 65535
            else:
                self.delta = delta - 65535
        else:
            self.delta = delta
            
        self.position += self.delta 
        
        return self.position, self.delta
    
    def get_position(self):
        '''
        @brief      Returns the most recently updated position of the encoder. 
        '''
        return self.position
    
    def set_position(self):
        '''
        @brief      Resets the position of the encoder to 0. 
        '''
        self.position = 0
        
        return self.position
    
    def get_delta(self):
        '''
        @brief      Returns the difference in recorded position between the two most 
                    recent calls to update(). 
        '''
        
        return self.delta
