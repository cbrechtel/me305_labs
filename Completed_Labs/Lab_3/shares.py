"""
@file shares.py

This file stores global variables used between two tasks. 

@author: Clara Brechtel

@date: 10/19/2020 
"""

## The command character sent from the user interface task to the cipher task
cmd     = None

## The response from the cipher task after encoding
resp    = None