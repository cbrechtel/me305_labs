"""
@file TaskUI.py

This file constructs the TaskUI object which reads user input and outputs the state
of the motor. 

@author: Clara Brechtel

@date: 10/19/2020 
"""

import shares
import utime
from pyb import UART

class TaskUI:
    '''
    @brief A TaskUI class that controls the User Interface. 
    @details This class reads input from the user at the serial port and 
            prints the response to the user. 
    
    @image 
    
    '''

    ## Initialization state
    S0_INIT = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD = 1
    
    ## If command present, interpret command 
    S2_WAIT_FOR_RESP = 2

    def __init__(self, taskNum, interval):
        '''
        @brief Creates a TaskUI object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The name of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        #self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Serial port
        self.ser = UART(2)


    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                # Run State 1 Code
                if self.ser.any():
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                    shares.cmd = self.ser.readchar()
                    
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code 
                if shares.resp:
                    print(shares.resp)
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
                    shares.resp = None
                
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState


    