"""
@file \Lab_3\main.py

This file runs two independent tasks, one tasks uses a motor encoder object and the other task
runs the user interface. 

@author: Clara Brechtel

@date: 10/19/2020 
"""
import pyb
from TaskEncoder import TaskEncoder, MotorEncoder
from TaskUI import TaskUI

pin1 = pyb.Pin.cpu.A6
pin2 = pyb.Pin.cpu.A7

## Create Motor Encoder object
Encoder = MotorEncoder(pin1, pin2, 3)

## User interface task, works with serial port
task0 = TaskEncoder(0, 1, Encoder, dbg=False)

## Cipher task encodes characters sent from user task by flipping the case of the character if it is a letter
task1 = TaskUI(1, 1, dbg=False)

## The task list contains the tasks to be run "round-robin" style
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()