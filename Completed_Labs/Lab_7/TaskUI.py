'''
@file \Lab_7\TaskUI.py
@brief User interface task as a finite state machine
@author Clara Brechtel
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date 12/04/2020
'''

import time
import serial
import matplotlib.pyplot as plt

class TaskUI:
    '''
    User interface task.
    
    An object of this class interacts with serial port UART2 waiting for 
    integers. Each integer recieved is read by the serial and placed into 
    an array, one array contains time values and the other array stores position.
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Read data from reference csv file
    S1_READ_DATA       = 1
    
    ## Recieve data over serial, parse data, and plot
    S2_PARSE_DATA       = 2
    
    ## Process and plot the data
    S3_PLOT_DATA        = 3 

    def __init__(self, taskNum, interval, dbg=True):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## List that contains reference times
        self.time = []
        
        ## List that includes reference velocitys
        self.velocity = []
        
        ## List that includes reference positions 
        self.position = []
        
        ## List that holds time read by the encoder
        self.actual_time = []
        
        ## List that contains actual velocity readings from the motor
        self.actual_velocity = []
        
        ## List that contains actual position of the motor shaft
        self.actual_position = []
        
        ## Find the actual start time and scale all the times to start at 0 seconds
        self.actual_start = 0 
        
        ## Initialize K, the total number of iterations, to 0 
        self.K = 0 
        
        ## Initialize J, the performance metric, to 0 
        self.J = 0 
        
        ## The number of seconds since Jan 1, 1970
        self.start_time = time.time()
        
        ## The "timestamp" for when to run the task next 
        self.next_time = self.start_time + self.interval 
        
        ## Serial port
        self.ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
        
        if self.dbg:
            print('Created user interface task')

    def run(self):
        '''
        Runs one iteration of the task
        '''

        ## Updates the current time stamp 
        self.curr_time = time.time()
        if (self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_READ_DATA)
                self.ser.reset_output_buffer()
                self.ser.reset_input_buffer()
                
            elif(self.state == self.S1_READ_DATA):
                self.printTrace()
                # Run State 1 Code
                ref = open('reference.csv')
                while True:
                    line = ref.readline()
                    if(len(self.time) == 15001):
                        break
                    else:
                        (t,v,x) = line.strip().split(',')
                        self.time.append(float(t))
                        self.velocity.append(float(v))
                        self.position.append(float(x))
                ref.close()
                self.ser.reset_input_buffer()
                self.transitionTo(self.S2_PARSE_DATA)

            elif(self.state == self.S2_PARSE_DATA):
                self.printTrace()
                # Run State 2 Code
                if (self.ser.inWaiting() > 100): 
                    for i in range(500):
                        line = self.ser.readline().decode('ascii')
                        (t, v, x) = line.strip('\r\n').split(',')
                        if t == 'None':
                            pass
                        else:
                            self.actual_time.append(float(t))
                            self.actual_velocity.append(float(v))
                            self.actual_position.append(float(x))
                    self.transitionTo(self.S3_PLOT_DATA)
                    
            elif(self.state == self.S3_PLOT_DATA):
                self.printTrace()
                # Run State 2 Code
                for i in range(len(self.actual_time)):
                    self.actual_time[i] -= self.actual_time[0]
                    
                for i in range(len(self.actual_position)):
                    if int(self.actual_position[i]) >= 150:
                        self.actual_start = i 
                        break
                    
                for i in range(self.actual_start):
                    del self.actual_time[i]
                    del self.actual_velocity[i]
                    del self.actual_position[i]
                
                self.start_time = self.actual_time[0]
                for i in range(len(self.actual_time)):
                    self.actual_time[i] -= self.start_time
                
                self.K = int(len(self.actual_time))
                for i in range(self.K):
                    self.J += (1/self.K) * ((self.velocity[i]-self.actual_velocity[i])**2 + (self.position[i]-self.actual_position[i] ** 2))

                fig, (ax1, ax2) = plt.subplots(2)
                ax1.plot(self.time, self.velocity, label='Reference')
                ax1.plot(self.actual_time, self.actual_velocity, 'tab:orange', label='Actual')
                ax1.set_title('Velocity and Position Profiles, J=%.2f' % self.J)
                ax1.set(ylabel='Angular Velocity [RPM]')

                ax2.plot(self.time, self.position)
                ax2.plot(self.actual_time, self.actual_position, 'tab:orange')
                ax2.set(xlabel='Time [s]', ylabel='Angular Position [deg]')
                
                ax1.legend(fancybox=True,loc='upper right')
                fig.show()
                fig.savefig("reference_response.png")
                self.transitionTo(self.S1_READ_DATA)
                self.time = []
                self.velocity = []
                self.position = []
                self.actual_time = []
                self.actual_velocity = []
                self.actual_position = []
                self.ser.flushInput()                
                self.ser.close()
                
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time += self.interval 
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, (self.curr_time - self.start_time))
            print(str)
            

        
    