"""
@file \Lab_7\Firmware\shares.py

This file stores global variables used between two tasks. 

@author: Clara Brechtel

@date: 12/04/2020 
"""

## The reference time value for one run 
time = None

## The reference velocity value for one run 
velocity = None

## The reference position for one run 
position = None

## The actual time of one run 
actual_time = None

## The actual velocity recorded for one run
actual_velocity = None

## The actual position recorded for one run
actual_position = None 