# -*- coding: utf-8 -*-
"""
@file \Lab_7\Firmware\MotorEncoder.py

@brief This class sets up a motor and defines the motors functionality. 

@author: Clara Brechtel

@date: 12/04/2020 
"""

import pyb 
import utime 

class MotorEncoder:
    '''
    @brief      A MotorEncoder class
    @details    This class represents an encoder on a motor. The motor is attached
                to two pins and one timer object. 
    '''
    
    
    def __init__(self, pin1, pin2, timer):
        '''
        @brief      Creates a motor object
        @param pin1  The first pin object that the motor is connected to
        @param pin2  The second pin object that the motor is connected to
        @param timer A timer object that the motor is connected to 
        '''
        
        ## The saved value of the previous tick count 
        self.prev_count = 0 
        ## The current tick count value 
        self.count = 0 
        ## The position of the encoder
        self.position = 0 
        ## The change in position of the encoder between two most recent updates
        self.delta = 0 
        ## The angular change in position of the motor, in degrees
        self.degrees = 0
        ## The angular change in position of the motor, in radians
        self.radians = 0 
        ## The speed of the motor 
        self.speed = 0 
        ## Speed in RPM's
        self.rpm = 0 
        ## The previous angular position of the motor, in radians 
        self.prev_degrees = 0 
        ## The time difference between calls to update, this is used to measure the speed 
        self.time_diff = 0 
        ## Intitialize previous time to current time 
        self.prev_time = utime.ticks_ms()
        ## Initialize current time to 0 
        self.curr_time = 0 
        
        ## The encoder timer object 
        self.tim=pyb.Timer(timer)
        self.tim.init(prescaler=0,period=65535)
        
        ## Connect the channels to the pins and timer 
        self.tim.channel(1,pin=pin1,mode=pyb.Timer.ENC_AB)
        self.tim.channel(2,pin=pin2,mode=pyb.Timer.ENC_AB)
        
        #print('Two pin objects created attached to pin '+ str(pin1) + ' and ' + str(pin2))
        #print('Timer object created attached to timer '+ str(timer))
    
    def update(self):
        '''
        @brief      Updates the recorded position, delta, angle, and speed of the encoder.
        '''
        self.curr_time = utime.ticks_ms()
        self.time_diff = utime.ticks_diff(self.curr_time, self.prev_time)*1E-3
        self.prev_time = self.curr_time
        self.prev_count = self.count
        self.prev_degrees = self.degrees 
        self.count = self.tim.counter()
        
        delta = self.count - self.prev_count
        
        # logic to fix 'bad' delta values 
        if (abs(delta) > 32767): 
            if(delta < 0):
                self.delta = delta + 65535
            else:
                self.delta = delta - 65535
        else:
            self.delta = delta
            
        self.position += self.delta 
        self.degrees = self.compute_angle()
        
        self.speed = (self.degrees - self.prev_degrees)/self.time_diff 
        
        # Speed in RPM
        self.rpm = self.speed/6
        
        return 
    
    def get_position(self):
        '''
        @brief      Returns the most recently updated position of the encoder. 
        '''
        return self.position
    
    def set_position(self, position):
        '''
        @brief      Resets the position of the encoder to 0. 
        @param position The value to reset the motor encoder position to. 
        '''
        self.tim.counter(position)
        self.position = position
        
        
        return
    
    def get_delta(self):
        '''
        @brief      Returns the difference in recorded position between the two most 
                    recent calls to update(). 
        '''
        
        return self.delta
    
    def compute_angle(self):
        '''
        @brief  Computes the angle using the position and 11.11 ticks per degree 

        '''
        
        degrees = (self.position/14.5528)
        self.radians = self.degrees * (3.14 / 180)
        
        return degrees 
    
    def compute_speed(self):
        '''
        @brief  Computes the measured angular velocity of the motor. 
        '''
        speed = abs((self.radians - self.prev_radians) / (self.time_diff))
        
        return speed 
        
    def get_speed(self):
        '''
        @brief Return current speed of the motor to the user.

        '''
        return self.speed 
        
    def get_angle(self):
        '''
        @brief Return angular position of the motor shaft in degrees. 
        '''
        return (0-self.degrees) 
        
    def get_rpm(self):
        '''
        @brief Return the speed in revolutions per minute (RPM).

        '''
        return self.rpm 