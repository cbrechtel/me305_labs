'''
@file \Lab_7\Firmware\TaskUser.py
@brief A task that communicates data between the front-end UI and the Nucleo board 
@author Clara Brechtel 
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date 12/04/2020
'''

import utime
from pyb import UART 
import shares 

class TaskUser:
    '''
    TaskData records and prints the time and position of the motor encoder. 
    
    An object of this class encodes the position and time from the encoder. 
    
    '''

    ## Initialization state
    S0_INIT = 0
    
    ## Wait for reference array to be present in buffer
    S1_READ_REF_DATA = 1
    
    ## Set the values for shares to communicate with the controller 
    S2_SET_SHARES = 2
    
    ## Recieve data from the controller 
    S3_RECIEVE_DATA = 3
    
    ## Send data to the front-end UI 
    S4_SEND_DATA = 4 
    
    def __init__(self, taskNum, interval, dbg=True):
        '''
        Creates a TaskData object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The name of the task
        self.taskNum = taskNum
        
        ##  The amount of time in miliseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Initialize reference time list
        self.time = [None]*501
        
        ## Initialize reference velocity list 
        self.velocity = [None]*501
        
        ## Initialize reference velocity list 
        self.position = [None]*501
        
        ## List that contains actual time readings from Nucleo
        self.actual_time = [None]*501
        
        ## List that contains actual velocity of the motor
        self.actual_velocity = [None]*501
        
        ## List that contains actual position of the motor shaft 
        self.actual_position = [None]*501
        
        ## Index for each line of the reference data 
        self.line = 0 
        
        ## Index for all of the lists 
        self.index = 0 
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Set up UART port 
        self.myuart = UART(2)

        if self.dbg:
            print('Created scaler task')
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_READ_REF_DATA)
            
            elif(self.state == self.S1_READ_REF_DATA):
                self.printTrace()
                # Run State 1 Code
                ref = open('reference.csv')
                while True:
                    line = ref.readline()
                    if(self.line == 15001):
                        break
                    elif(self.line % 30 == 0):
                        (t,v,x) = line.strip().split(',')
                        self.time[self.index] = float(t)
                        self.velocity[self.index] = float(v)
                        self.position[self.index] = float(x)
                        self.index += 1 
                    else:
                        pass 
                    self.line += 1 
                ref.close()
                self.transitionTo(self.S2_SET_SHARES)
                self.index = 0 
            
            elif(self.state == self.S2_SET_SHARES):
                self.printTrace()
                # Run State 2 Code 
                if (self.index < len(self.time)):
                    shares.time = self.time[self.index]
                    shares.velocity = self.velocity[self.index]
                    #print(shares.velocity)
                    shares.position = self.position[self.index]
                    self.transitionTo(self.S3_RECIEVE_DATA)
                else:
                    self.transitionTo(self.S4_SEND_DATA)
                    shares.time = None
                    shares.velocity = None
                    shares.position = None
                    
            elif(self.state == self.S3_RECIEVE_DATA):
                self.printTrace()
                # Run state 3 code 
                self.actual_time[self.index] = shares.actual_time
                self.actual_velocity[self.index] = shares.actual_velocity
                self.actual_position[self.index] = shares.actual_position
                self.index += 1
                self.transitionTo(self.S2_SET_SHARES)
                
            elif(self.state == self.S4_SEND_DATA):
                self.printTrace()
                # Run state 4 code 
                for i in range(len(self.actual_time)):
                    #print('{},{},{}\r\n'.format(self.actual_time[i],self.actual_velocity[i], self.actual_position[i]))
                    self.myuart.write('{},{},{}\r\n'.format(self.actual_time[i],self.actual_velocity[i], self.actual_position[i]))
                self.transitionTo(self.S1_READ_REF_DATA)
                
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
    
