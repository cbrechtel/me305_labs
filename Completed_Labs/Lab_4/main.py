"""
@file \Lab_4\main.py

This file runs two independent tasks, one tasks uses a motor encoder object and the other task
runs the user interface. 

@author: Clara Brechtel

@date: 10/19/2020 
"""

from TaskUIFront import TaskUIFront


## Cipher task encodes characters sent from user task by flipping the case of the character if it is a letter
task0 = TaskUIFront(1, 0.1, dbg = False)

## The task list contains the tasks to be run "round-robin" style
taskList = [task0]

while True:
    for task in taskList:
        task.run()