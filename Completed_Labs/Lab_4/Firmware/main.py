"""
@file \Lab_4\Firmware\main.py

This file is uploaded onto the Nucleo microcontroller and creates an object of the Motor encoder. 
The pins used in TaskData are defined in this file. This file causes the TaskData.py file to 
run immediatly on power up. 

@author: Clara Brechtel

@date: 11/02/2020 

"""
import pyb
from TaskData import TaskData, MotorEncoder

## Create pin object at pin A6
pin1 = pyb.Pin.cpu.A6
## Create pin object at pin A7
pin2 = pyb.Pin.cpu.A7

## Create Motor Encoder object
Encoder = MotorEncoder(pin1, pin2, 3)


## (Task 1) TaskData records and prints the position of the encoder at each time point
task1 = TaskData(1, 1_000, Encoder, dbg = False)

## The task list contains the tasks to be run "round-robin" style
taskList = [task1]

while True:
    for task in taskList:
        task.run()