'''
@file TaskData.py
@brief A task that colllects position data from a motor encoder obect and sends the data to a front end UI
@author Clara Brechtel 
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date November 2, 2020
'''

import utime
from pyb import UART 
import pyb

class TaskData:
    '''
    TaskData records and prints the time and position of the motor encoder. 
    
    An object of this class encodes the position and time from the encoder. 
    
    '''

    ## Initialization state
    S0_INIT = 0
    
    ## Wait for command to start collecting data 
    S1_WAIT_FOR_START = 1
    
    ## Compute data 
    S2_COMPUTE_DATA = 2 
    
    ## Print data
    S3_PRINT_DATA = 3
    
    def __init__(self, taskNum, interval, MotorEncoder, dbg=True):
        '''
        Creates a TaskData object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The name of the task
        self.taskNum = taskNum
        
        ## A class attribute "copy" of the Motor Encoder object 
        self.MotorEncoder = MotorEncoder
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Initialize empty time matrix, pre-allocate size for speed
        self.time = [None]*51
        
        ## Initialize empty position matrix, pre-allocate size for speed 
        self.position = [None]*51
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Set up UART port 
        self.myuart = UART(2)
        
        if self.dbg:
            print('Created scaler task')

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_START)
            
            elif(self.state == self.S1_WAIT_FOR_START):
                self.printTrace()
                # Run State 1 Code
                if(self.myuart.any()):
                    val = self.myuart.readchar()
                    #self.myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo \r\n')
                    if(val == 103):
                        self.transitionTo(self.S2_COMPUTE_DATA)
                        val = None
                         
            elif(self.state == self.S2_COMPUTE_DATA):
                self.printTrace()
                # Run State 2 Code 
                for i in range(len(self.time)):
                    utime.sleep_ms(200)
                    time, position = self.MotorEncoder.compute_data()
                    self.time[i] = time
                    self.position[i] = position
                    if (i == 50):
                        self.transitionTo(self.S3_PRINT_DATA)
                    elif (self.myuart.any()):
                        val = self.myuart.readchar()
                        if(val == 115):
                            self.transitionTo(self.S3_PRINT_DATA)
                            val = None
                            break
                        break
                
                            
            
            elif(self.state == self.S3_PRINT_DATA):
                self.printTrace()
                # Run State 3 Code 
                for j in range(len(self.time)):
                    self.myuart.write('{:},{:}\r\n'.format(self.time[j],self.position[j]))
                self.transitionTo(self.S1_WAIT_FOR_START)
                self.time = [None]*51
                self.position = [None]*51
                
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
    
class MotorEncoder:
    '''
    @brief      A MotorEncoder class
    @details    This class represents an encoder on a motor. The motor is attached
                to two pins and one timer object. 
    '''
    
    
    def __init__(self, pin1, pin2, timer):
        '''
        @brief      Creates a motor object
        @param pin1  The first pin object that the motor is connected to
        @param pin2  The second pin object that the motor is connected to
        @param timer A timer object that the motor is connected to 
        '''
        
        ## The position of the encoder
        self.position = 0 
        ## The change in position of the encoder between two most recent updates
        self.delta = 0 
        
        ## The encoder timer object 
        self.tim=pyb.Timer(timer)
        self.tim.init(prescaler=0,period=65535)
        
        ## Connect the channels to the pins and timer 
        self.tim.channel(1,pin=pin1,mode=pyb.Timer.ENC_AB)
        self.tim.channel(2,pin=pin2,mode=pyb.Timer.ENC_AB)
        
        print('Two pin objects created attached to pin '+ str(pin1) + ' and ' + str(pin2))
        print('Timer object created attached to timer '+ str(timer))
    
    def compute_data(self):
        '''
        @brief      Updates the recorded position and time  the encoder.
        '''
        
        curr_time = utime.ticks_us()
        #prev_position = self.position
        count = self.tim.counter()
        delta = count - self.position
        # logic to fix 'bad' delta values 
        if (abs(delta) > 32767): 
            if(delta < 0):
                self.delta = delta + 65535
            else:
                self.delta = delta - 65535
        else:
            self.delta = delta
            
        self.position += self.delta 
        ## Variable storing the time for each index 
        self.time = curr_time

        return self.time, self.position