'''
@file TaskUIFront.py
@brief User interface task as a finite state machine
@author Clara Brechtel
@copyright Copywrite info coming soon! 
'''

import time
import serial
import matplotlib.pyplot as plt
import fastnumbers
import numpy as np

class TaskUIFront:
    '''
    User interface task.
    
    An object of this class interacts with serial port UART2 waiting for 
    integers. Each integer recieved is read by the serial and placed into 
    an array, one array contains time values and the other array stores position.
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_QUERY_USER       = 1
        
    ## Parse data 
    S2_PARSE_DATA       = 2

    def __init__(self, taskNum, interval, dbg=True):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## Initialize empty array to hold time values 
        self.enc_time = []
        
        ## Initialize empty array to hold position values
        self.position = []
        
        ## Holds the user input variable
        self.myval = 0 
        
        ## The number of seconds since Jan 1, 1970
        self.start_time = time.time()
        
        ## The "timestamp" for when to run the task next 
        self.next_time = self.start_time + self.interval 
        
        ## Serial port
        self.ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
        
        if self.dbg:
            print('Created user interface task')

    def run(self):
        '''
        Runs one iteration of the task
        '''

        ## Updates the current time stamp 
        self.curr_time = time.time()
        if (self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_QUERY_USER)
            
            elif(self.state == self.S1_QUERY_USER):
                self.printTrace()
                # Run State 1 Code
                self.myval = self.sendChar()
                if(self.myval == 'g'):
                    print('Computing encoder positions...')
                    self.myval = self.stopChar()
                    if(self.myval == 's'):
                        self.transitionTo(self.S2_PARSE_DATA)
                        self.myval = None
                    
            elif(self.state == self.S2_PARSE_DATA):
                self.printTrace()
                # Run State 2 Code
                end = False
                waiting = self.ser.inWaiting() 
                if (waiting > 2): 
                    for i in range(51):
                        enc_time, position = self.collect_data()
                        self.enc_time.append(enc_time)
                        self.position.append(position)
                        #print(i)
                        if (i == 50 or self.enc_time[i] == 'Stop'):
                            print(i)
                            end = True
                            waiting = 0     
                            break
                else:
                    pass
                
                if end:
                    self.enc_time.pop(i)
                    self.position.pop(i)
                    plt.plot(self.enc_time, self.position)
                    plt.title("Encoder Position Over Time")
                    plt.xlabel("Time (micro-seconds)")
                    plt.ylabel("Encoder Position")
                    plt.show()
                    plt.savefig("motor_encoder_data.png")
                    np.asarray(self.enc_time)
                    np.asarray(self.position)
                    np.savetxt('encoder_data.csv', (self.enc_time, self.position), delimiter=',')
                    #self.ser.close()
                    self.transitionTo(self.S1_QUERY_USER)
                    self.enc_time = []
                    self.position = []
                    self.ser.flushInput()
        
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time += self.interval 
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, (self.curr_time - self.start_time))
            print(str)
            
    def sendChar(self):
        '''
        Query user for input 
        '''
        
        myval = input('Input "g" to start computing data from the encoder: ')
        self.ser.write(str(myval).encode('ascii'))
     
        return myval 
    
    def stopChar(self):
        '''
        Query user to stop recording data.

        '''

        myval = input('Input "s" to stop recording data: ')
        self.ser.write(str(myval).encode('ascii'))
     
        return myval 
        
    
    def collect_data(self):
        '''
        Reads data from serial and converts it to an integer value. 

        '''
        
        line_string = self.ser.readline().decode('ascii')
        line_list = line_string.strip('\r\n').split(',')
        print(line_list)
        if line_list[0] == 'None':
            enc_time = 'Stop'
        elif fastnumbers.isfloat(line_list[0], str_only=False):
            enc_time = float(line_list[0])     
        else:
            pass
            
        if line_list[1] == 'None':
            position = 'Stop'
        elif fastnumbers.isfloat(line_list[1], str_only=False):
            position = float(line_list[1])
        else:
            pass
        
        return enc_time, position