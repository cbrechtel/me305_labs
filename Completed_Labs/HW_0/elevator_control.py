'''
@file elevator_control.py

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary elevator.

The user has two buttons, Button_1 and Button_2, to move to the first and second floor, respectively. 

There is also a sensor on each floor that indicates if the elevator is on First or Second floor. 
'''

import utime 

class TaskElevator:
    '''
    @brief      A finite state machine to control an elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator with two floors.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT = 0
    ## Constant defining State 1 - Moving Down
    S1_MOVING_DOWN = 1
    ## Constant defining State 2 - Moving Up 
    S2_MOVING_UP = 2
    ## Constant defining State 3 - Stopped at floor 1
    S3_STOPPED_FLOOR_1 = 3
    ## Constant defining State 4 - Stopped at floor 2 
    S4_STOPPED_FLOOR_2 = 4
    
    def __init__(self, interval, Motor, Button_1, Button_2, First, Second):
        '''
        @brief      Creates a TaskElevator object and initializes state. 
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object 
        self.Motor = Motor  # Stores class copy of Motor so other functions can use motor object 
        
        ## A class attribute "copy" of the "Button_1" variable 
        self.Button_1 = Button_1
        
        ## A class attribute "copy" of the "Button_2" variable 
        self.Button_2 = Button_2
        
        ## A class attribute "copy" of the "First" variable 
        self.First = First
        
        ## A class attribute "copy" of the "Second" variable 
        self.Second = Second
        
        ## A counter showing the number of times a task is run 
        self.runs = 0
        
        ## The number of seconds since Jan 1, 1970
        self.start_time = utime.ticks.us()
        
        ## The interval of time, in seconds, betweens runs of the task 
        self.interval = int(interval*1e6)
        
        ## The "timestamp" for when to run the task next 
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Updates the current time stamp 
        self.curr_time = utime.ticks_us()
        
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.Button_1.Clear()
                self.Button_2.Clear()
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.Downwards()
                
            elif(self.state == self.S1_MOVING_DOWN):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                if self.First.OnFirst():
                    self.Button_1.Clear()
                    self.transitionTo(self.S3_STOPPED_FLOOR_1)
                    self.Motor.Stationary()
                              
            elif(self.state == self.S2_MOVING_UP):
                print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 2 Code   
                if self.Second.OnSecond():
                    self.Button_2.Clear()
                    self.transitionTo(self.S4_STOPPED_FLOOR_2)
                    self.Motor.Stationary()
                          
            elif(self.state == self.S3_STOPPED_FLOOR_1):
                print(str(self.runs) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                if self.Button_2.GoToSecond():
                    self.transitionTo(self.S2_MOVING_UP) # Changes state variable 
                    self.Motor.Upwards()
                                    
            elif(self.state == self.S4_STOPPED_FLOOR_2):
                print(str(self.runs) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                if self.Button_1.GoToFirst():
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.Downwards()
                    
            else:
                # Uh-oh state (undefined)
                # Error handling 
                pass 
        
            # Add runs here 
            self.runs += 1 
            
            self.next_time += utime.ticks_add(self.next_time, self.interval)   # Updating the "scheduled" timestamp 
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        


class Button:
    '''
    @brief      A button class
    @details    This class represents a button that the can be pushed by the
                imaginary person to move the elevator to the first or second floor. 
                As of right now this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def Clear(self):
        '''
        @brief      Clear all input buttons.
        '''
        return 0 
    
    def GoToFirst(self):
        '''
        @brief      Moves elelvator to first floor. 
        '''
        return 1  
    
    def GoToSecond(self):
        '''
        @brief      Moves elevator to second floor. 
        '''
        return 1 
    
class Sensor:
    '''
    @brief      A sensor class
    @details    This class represents two proximity sensors that provide logic to the 
                elevator to indicate if the elevator is on the first or second floor. 
                As of right now this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Sensor object
        @param pin  A pin object that the sensor is connected to
        '''
        ## The pin object used to read the Sensor state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))
        
    def OnFirst(self):
        '''
        @brief      Elevator on first floor. 

        '''
        return 1 
    
    def OnSecond(self):
        '''
        @brief      Elevator on second floor. 
        '''
        return 1 



class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator 
                move up and down.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Stationary(self):
        '''
        @brief Stops the motor
        '''
        print('Motor stationary')
        return 0 
    
    def Upwards(self):
        '''
        @brief Moves the motor upwards
        '''
        print('Motor moving up')
        return 1
    
    def Downwards(self):
        '''
        @brief Moves the motor downwards
        '''
        print('Motor moving down')
        return 2

    