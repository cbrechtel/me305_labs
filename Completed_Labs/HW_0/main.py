 
# -*- coding: utf-8 -*-
"""
@file \HW_0\main.py

This file constructs the objects defined in elevator_control.py and runs two 
independent tasks with the elevator. 

@author: Clara Brechtel 
"""
from elevator_control import Button, Sensor, MotorDriver, TaskElevator


## (Task 1) Create motor object 
Motor1 = MotorDriver()

## (Task 1) Button Object for Button 1 
Button1_1 = Button('PB6')

## (Task 1) Button Object for Button 2 
Button1_2 = Button('PB7')

## (Task 1) Sensor Object for First Floor 
First1 = Sensor('PB8')

## (Task 1) Sensor Object for Second Floor 
Second1 = Sensor('PB9')

## (Task 2) Create motor object 
Motor2 = MotorDriver()

## (Task 2) Button Object for Button 1 
Button2_1 = Button('PB7')

## (Task 2) Button Object for Button 2 
Button2_2 = Button('PB8')

## (Task 2) Sensor Object for First Floor 
First2 = Sensor('PB9')

## (Task 2) Sensor Object for Second Floor
Second2 = Sensor('PB10')

# Create Task object
## First task object that runs independently 
task1 = TaskElevator(0.1, Motor1, Button1_1, Button1_2, First1, Second1)  # Will also run constructor 
## Second task object that runs independently from task 1 
task2 = TaskElevator(0.1, Motor2, Button2_1, Button2_2, First2, Second2)

# To run the task, call task1.run() over and over 
for N in range(10000000):    # Will change to "While True" 
    task1.run()
    task2.run()