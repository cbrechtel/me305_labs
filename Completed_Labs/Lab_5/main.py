# -*- coding: utf-8 -*-
"""
@file main.py
@brief This program creates a UserLED object and then instantiates the TaskBLE to run on a Nucleo pyboard at power up. 
@author Clara Brechtel 
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date November 9, 2020
"""

from TaskBLE import TaskBLE, UserLED
import pyb

## User specified UART used for communication across BLE 
uart = 3

## User specified pin that is attached to the LED you want to blink
pin = pyb.Pin.cpu.A5

## User specifed timer that is attached to the LED pin 
timer = 4

## Create a UserLED object from the UserLED class
UserLED = UserLED(pin, timer)

## (Task 1) TaskData records and prints the position of the encoder at each time point
task1 = TaskBLE(1, uart, 1, UserLED, dbg=True)

## The task list contains the tasks to be run "round-robin" style
taskList = [task1]

while True:
    for task in taskList:
        task.run()


