# -*- coding: utf-8 -*-
"""
@file TaskBLE.py
@brief This program contains TaskBLE and a UserLED class. TaskBLE creates a copy of an object of
UserLED and allows a user to control the LED by communicating through a Bluetooth Low-Energy device. 
@author Clara Brechtel 
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date November 9, 2020
"""

import utime 
from pyb import UART
import pyb

class TaskBLE:
    '''
    Represents a BLE module driver. Allows us to read, write to, and check if 
    any characters are waiting in UART and control a user LED object. 
    '''
    ## A class variable representing State 0
    S0_INIT = 0
    
    ## A class variable representing State 1
    S1_WAIT_FOR_CMD = 1
    
    ## A class variable representing State 2
    S2_READ_CMD = 2
    
    
    def __init__(self, taskNum, uart_num, interval, UserLED, dbg=True):
        '''
        @brief  Creates a UserLED object, creates a uart object, and initializes states

        '''
        ## The name of the task 
        self.taskNum = taskNum 
        
        ## Flag to print debug messages or suppress them 
        self.dbg = dbg
        
        ## Create a class attribute 'copy' of the UserLED object at pin_led location 
        self.UserLED = UserLED
        
        ## The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The state to run on the next iteration of the task 
        self.state = self.S0_INIT
        
        ## Counter that keeps track of amount of times the task has run 
        self.runs = 0 
        
        ## The timestamp for the first iteration 
        self.start_time = utime.ticks_us()
        
        ## The timestamp for when the task should run next 
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Assign UART port number and initialize UART port 
        self.myuart = UART(uart_num, 9600)
        self.myuart.init(9600, bits=8, parity=None, stop=2)
        print('UART #{} Initialized.'.format(uart_num))
        
        ## Initialize waiting to False
        self.characters_present = False
        
        ## Initialize LED frequency to None 
        self.frequency = None 
        
    def run(self):
        '''
        Runs one iteration of the task 
        '''
        ## The timestamp for the most recent iteration 
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            if(self.state == self.S0_INIT):
                self.printTrace()
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                
            elif(self.state == self.S1_WAIT_FOR_CMD):
                self.printTrace()
                self.myuart.write('Set LED blinking frequency (Hz).\r\n')
                self.characters_present = self.uartWait()
                if self.characters_present:
                    self.transitionTo(self.S2_READ_CMD)
                    self.frequency = self.uartRead()
                    self.characters_present = False
                    
            elif(self.state == self.S2_READ_CMD):
                self.printTrace()
                if self.frequency != None:
                    if self.frequency > 0: 
                        self.UserLED.setFreq(self.frequency)
                    elif self.frequency == 0:
                        self.UserLED.zeroFreq()
                    else:
                        pass
                    self.uartWrite()
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
                    self.frequency = None 
                
            else:
                # Invalid state code, error handling
                pass
            
            self.runs += 1 
            
            ## Specifying the next time the task will run 
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable 

        '''
        self.state = newState
    
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
            
    def uartWait(self):
        '''
        @brief Wait for character to be present in UART, return True once present

        '''
        run = True 
        while run:
            if(self.myuart.any() != 0):
                run = False
                return True
            else:
                pass
            
    def uartRead(self):
        '''
        @brief Read characters waiting in serial, handles values that are not integers. 
        '''
        value = self.myuart.read()
        value.decode()
        try:
            value = int(value)
        except ValueError:
            self.myuart.write("Error! Input integer only.\r\n")
            value = -1
        
        return value 
    
    def uartWrite(self):
        '''
        @brief Writes state of LED to the user 

        '''
        if self.frequency > 0:
            self.myuart.write('LED on and blinking at {} Hz.\r\n'.format(self.frequency))
        elif self.frequency == 0:
            self.myuart.write('LED turned off.\r\n')
        
class UserLED:
    '''
    @brief      A UserLED class
    @details    This class represents an LED located on the Nucleo board. The 
                LED is attached to one pin on the Nucleo. 
    '''
    
    def __init__(self, pin, timer):
        '''
        @brief      Creates a UserLED object 
        @param pin  The pin object that the LED is attached to 
        @param timer The timer connected to the pin that will be used to set blinking frequency
        '''
        
        ## Define LED pin location and set pin as output 
        self.pin = pyb.Pin(pin, pyb.Pin.OUT_PP)
        
        ## Create a timer object with given timer number 
        self.tim = pyb.Timer(int(timer))
        
    def turnOn(self):
        '''
        @brief  Turns on LED

        '''
        self.pin.high()
        return
        
    def turnOff(self):
        '''
        @brief  Turns off LED 

        '''
        self.pin.low()
        return
        
    def toggle(self):
        '''
        @brief  Toggles LED on and off, set delay to 8ms in order to visually see LED blinking

        '''
        
        self.turnOn()
        pyb.delay(8)
        self.turnOff()
        return 
        
    def setFreq(self, freq):
        '''
        @brief Uses the timer to set the frequency of the LED toggle
        @param freq Input frequency specified by the user 

        '''
        self.tim.init(freq=freq)
        self.tim.callback(lambda t:self.toggle())
        return 
    
    def zeroFreq(self):
        '''
        @brief De-initializes callback function of timer object 

        '''
        self.tim.callback(None)
        self.turnOff()
        return