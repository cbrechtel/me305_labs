'''
@file \Lab_1\main.py
@brief A file that runs the Fibonacci calculator. 
@details The main_fibonacci.py file runs the Fibonacci calculator by first prompting the 
user for an input and then uses the Fibonacci package to calculate the Fibonacci 
number at that specified index.This file continues to run the Fibonacci calculator 
for an infinite loop until the user decides to exit the program by typing 'x'. 
The calculator only accepts a valid index input from the user. 

@author Clara Brechtel 

@date September 22, 2020

'''

import fibonacci as fib

## Determines index location of calculated Fibonacci number 
idx = float(input('Please enter positive integer to start Fibonacci sequence: '))
idx = fib.validate_input(idx)
fib.fib(idx)
while True: # Repeat program until user exits
    idx = input('Please enter another start integer or type x to exit program: ')
    if idx == 'x':
        print('Exiting Program.')
        break
    else:
        idx = fib.validate_input(float(idx))
        fib.fib(idx)
        
        