'''
@file fibonacci.py
@brief Calculates a Fibonacci number. 
@details This fibonacci.py file contains two methods, validate_input(idx) and fib(idx). These functions
take a parameter named 'idx' which indicates the index value of the Fibonacci
sequence where the Fibonacci value will be calculated and returned to the user. 

@author Clara Brechtel 
 
@date September 22, 2020

'''

def validate_input(idx):
    '''
    @brief This method validates that the user input, idx, is a valid integer. 
    @details The method asks for a new input if the user enters an invalid value. 
    @param idx is an input provided by the user in main_fibonacci 
    '''
    while True:
        if (idx % 1 == 0) & (idx >= 0):
            idx = int(idx)
            break
        else:
            idx = float(input('Error! Please enter a positive integer: '))
    return idx 


def fib(idx):
    ''' This method calculates a Fibonacci number corresponding to a 
    specified index. 
    @details The program creates a list of the length of idx + 1 and calculates 
    the Fibonacci number at each index of the list. The number at the index location 
    of idx is returned. 
    @param idx is a valid integer that is returned by fibonacci.validate_input 
    
    '''
    
    print('Calculating Fibonacci number at '
          'index n = {:}.'.format(idx))
    f_seq = list(range(idx+1))
    for i in f_seq: 
        if i > 1:
            f_seq[i] = f_seq[i-1] + f_seq[i-2]
    fib_num = f_seq[idx]
    print('Fibonacci number: ' + str(fib_num))
    return fib_num
        

if __name__=='__main__':
    fib()
    validate_input()