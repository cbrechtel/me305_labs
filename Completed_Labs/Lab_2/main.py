# -*- coding: utf-8 -*-
"""
@file \Lab_2\main.py

This file constructs the objects defined in LED_control.py and runs two 
independent tasks, one with a 'virtual' LED and one with a real LED. 

@author: Clara Brechtel

@date: 10/12/2020 
"""

from LED_control import VirtualLED, vLED, RealLED, rLED
import pyb 

## Create pin object as pin_A5 
pin_A5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
## Create timer object 
tim = pyb.Timer(2, freq=100)

## (Task 1) Create Virtual LED object 
LED1 = vLED('pin 1')

## (Task 2) Create Real LED object 
LED2 = rLED(pin_A5, tim)

# Create Task object
## First task object that runs independently 
task1 = VirtualLED(.1, LED1)  # Will also run constructor 
## Second task object that runs independently 
task2 = RealLED(1, LED2)

# To run the task, call task1.run() over and over 
while True:    # Will change to "While True" 
    task1.run()
    task2.run()


