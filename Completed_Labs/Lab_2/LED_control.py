# -*- coding: utf-8 -*-
'''
@file LED_control.py

This file implements a finite state machine for two tasks. One task blinks a 'virtual'
LED on and off by printing 'LED ON' and 'LED OFF'. The other task creates a real object 
to change the physical LED brightness on a Nucleo board. 

@author: Clara Brechtel 

@date
'''

import time 
import utime 
import pyb 

class VirtualLED:
    '''
    @brief      A finite state machine to control a virtual LED.
    @details    This class implements a finite state machine to control a virtual LED
                to turn on and off.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT = 0
    ## Constant defining State 1 - LED Off 
    S1_LED_ON = 1
    ## Constant defining State 2 - LED On 
    S2_LED_OFF = 2
 
    
    def __init__(self, interval, vLED):
        '''
        @brief      Creates a VirtualLED object and initializes state. 
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the LED object 
        self.vLED = vLED
        
        ## A counter showing the number of times a task is run 
        self.runs = 0
        
        ## The number of seconds since Jan 1, 1970
        self.start_time = time.time()
        
        ## The interval of time, in seconds, betweens runs of the task 
        self.interval = 0.1
        
        ## The "timestamp" for when to run the task next 
        self.next_time = self.start_time + self.interval 
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Updates the current time stamp 
        self.curr_time = time.time()
        
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.transitionTo(self.S1_LED_ON)
                self.vLED.TurnOn()
                
            elif(self.state == self.S1_LED_ON):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                time.sleep(0.5)
                self.transitionTo(self.S2_LED_OFF)
                self.vLED.TurnOff()
             
            elif(self.state == self.S2_LED_OFF):
                print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                time.sleep(0.5)
                self.transitionTo(self.S1_LED_ON)
                self.vLED.TurnOn()
                
            else:
                # Uh-oh state (undefined)
                # Error handling 
                pass 
        
            # Add runs here 
            self.runs += 1 
            
            self.next_time += self.interval   # Updating the "scheduled" timestamp 
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class vLED:
    '''
    @brief      A virtual LED class
    @details    This class represents 'virtual' LED that turns on and off between states.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a virtual LED object
        @param pin  A pin object that the virtual LED is connected to
        '''
        
        ## The pin object used to read the LED state
        self.pin = pin
        
        print('Virtual LED object created attached to pin '+ str(self.pin))

    
    def TurnOn(self):
        '''
        @brief      Turn the virtual LED on.
        '''
        return print('LED ON')
    
    def TurnOff(self):
        '''
        @brief      Turn the virtual LED off. 
        '''
        return print('LED OFF')
    
class RealLED:
    '''
    @brief      A finite state machine to control a real LED on the Nucleo.
    @details    This class implements a finite state machine to control the
                brightness of an LED.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT = 0
    ## Constant defining State 1 - LED Off 
    S1_LED_RAMP_UP = 1 
    ## Constant defining State 2 - LED On 
    S2_LED_OFF = 2
 
    
    def __init__(self, interval, rLED):
        '''
        @brief      Creates a RealLED object and initializes state. 
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the LED object 
        self.rLED = rLED
        
        ## A counter showing the number of times a task is run 
        self.runs = 0
        
        ## The number of seconds since Jan 1, 1970
        self.start_time = utime.ticks_us()
        
        ## The interval of time, in seconds, betweens runs of the task 
        self.interval = int(interval*1e6)
        
        ## The "timestamp" for when to run the task next 
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Updates the current time stamp 
        self.curr_time = utime.ticks_us()
        
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.transitionTo(self.S1_LED_RAMP_UP)
                for i in range(10):
                    self.rLED.RampUp(10*i)
                    utime.sleep(0.1)
                    
            elif(self.state == self.S1_LED_RAMP_UP):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                self.transitionTo(self.S2_LED_OFF)
                self.rLED.TurnOff()
                utime.sleep(0.1)
             
            elif(self.state == self.S2_LED_OFF):
                print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                self.transitionTo(self.S1_LED_RAMP_UP)
                for i in range(10):
                    self.rLED.RampUp(10*i)
                    utime.sleep(0.1)
                
            else:
                # Uh-oh state (undefined)
                # Error handling 
                pass 
        
            # Add runs here 
            self.runs += 1 
            
            self.next_time += utime.ticks_add(self.next_time, self.interval)   # Updating the "scheduled" timestamp 
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        


class rLED:
    '''
    @brief      A real LED class
    @details    This class represents a real LED that is located on the Nucleo microcontroller.
    '''
    
    def __init__(self, pin, tim):
        '''
        @brief      Creates a Real LED object
        @param pin  A pin object that the LED is connected to
        @param tim  A timer that the LED is connected to 
        '''
        
        ## The pin object used to read the LED state
        self.pin = pin
        ## The timer object used to keep time on the Nucleo
        self.tim = tim
        ## The channel object used to generate PWM signal
        self.t1ch1 = self.tim.channel(1, pyb.Timer.PWM, pin=self.pin)
        
        print('LED object created attached to pin '+ str(self.pin))

    
    def RampUp(self, i):
        '''
        @brief      Gradually increases LED to full brightness.
        '''
            
        return self.t1ch1.pulse_width_percent(i)
    
    def TurnOff(self):
        '''
        @brief      Turns off LED. 
        '''
        
        return self.t1ch1.pulse_width_percent(0)
    
